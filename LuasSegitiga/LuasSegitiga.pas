//fpc 3.0.0
{ Program membaca alas (a) dan tinggi (t) sebuah segitiga, menghitung luas
  segitiga dengan rumus simple, lalu menampilkannya di layar.}

program LuasSegitiga;

{ DEKLARASI }
const two: real = 2.0;
var alas: real;
    tinggi: real;
    Luas: real;

{ ALGORITMA }
begin
    readln(alas, tinggi);
    Luas := alas * tinggi / two;
    writeln('Luas segitiga adalah', Luas);
end.
