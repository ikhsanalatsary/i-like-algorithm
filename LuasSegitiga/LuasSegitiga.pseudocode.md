# Luas Segitiga

## Notasi

```
  PROGRAM Luas Segitiga
  { Program membaca alas (a) dan tinggi (t) sebuah segitiga, menghitung luas
  segitiga dengan rumus simple, lalu menampilkannya di layar.}
  
  DEKLARASI:
    const double = 2;
    a: real;    { panjang alas segitiga, dalam satuan cm }
    t: real:    { tinggi segitiga, dalam satuan cm }
    Luas: real; { luas segitiga, dalam satuan cm² }
    
  ALGORITMA:
    read(a, t)            { inputkan panjang dan lebar segitiga }
    Luas ← a * t / double { hitung luas segitiga }
    write(Luas)           { tampilkan luas segitiga ke layar }
```