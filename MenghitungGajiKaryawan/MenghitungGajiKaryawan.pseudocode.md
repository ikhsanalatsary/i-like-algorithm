# Menghitung Gaji Karyawan

## Masalah
Seorang karyawan menerima gaji berupa gaji pokok dan tunjangan. Penghasilan tersebut harus dikurangi lagi dengan besar pajak. tulislah algoritma yang membaca nama karyawan dan gaji pokok bulananny, lalu menghitung gaji bersih karyawan tersebut, dan menampilkannya nama karyawan beserta gaji bersihnya. Gaji bersih yang diterima pegawai adalah:

``` 
Gaji bersih = gaji pokok + tunjangan - pajak
```

Tunjangan karyawan dihitung 20% dari gaji pokok, sedangkan pajak adalah 15% dari gaji pokok ditambah tunjangan. Kemudian Gaji bersih karyawan dicetak ke layar.

##  Notasi
```
PROGRAM GajiBersihKaryawan
{ Menghitung gaji bersih karyawan. Data masukan adalah nama karyawan dan gaji pokok bulanannya. Gaji bersih gaji pokok + tunjangan - pajak. Tunjangan adalah 20% dari gaji pokok, sedangkan pajak adalah 15% dari gaji pokok. Luarannya adalah nama karyawan dan gaji bersihnya }

DEKLARASI:
const persenTunjangan = 0.2 { persentase tunjangan gaji }
const persenPajak = 0.15 { persentase potongan pajak }
NamKaryawan : string
GajiPokok, GajiBersih, tunjangan, pajak : real

ALGORITMA:
read(NamaKaryawan, GajiPokok)
tunjangan ← PersenTunjangan * GajiPokok
pajak ← PersenPajak * (GajiPokok + tunjangan)
GajiBersih ← GajiPokok + tunjangan - pajak
write(NamaKaryawan, GajiBersih)
```