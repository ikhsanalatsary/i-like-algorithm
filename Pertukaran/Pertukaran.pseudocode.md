# Pertukaran

## Notasi
```
PROGRAM Pertukaran
  { Mempertukarkan nilai A dan B }
  
  DEKLARASI:
    A, B, C : integer

  ALGORITMA:
    { Misalkan A diisi 8 dan B diisi 5 }
    A ← 8; B ← 5;

    { Cetak dulu nilai A dan B sebelum pertukaran }
    write(A, B)

    { Pertukaran nilai A dan B }
    C ← A { Tampung nilai A ke dalam C }
    A ← B { Pindahkan nilai B ke dalam A }
    B ← C { Pindahkan nilai A yang tersimpan di C ke dalam B }

    { cetak nilai A dan B hasil pertukaran }
    write(A, B)
```

### Cara lain
Cara ini hanya bisa dilakukan jika nilai A dan B adalah angka.

```
PROGRAM Pertukaran
  { Mempertukarkan nilai A dan B }
  
  DEKLARASI:
    A, B : integer

  ALGORITMA:
    { Misalkan A diisi 8 dan B diisi 5 }
    A ← 8; B ← 5;

    { Cetak dulu nilai A dan B sebelum pertukaran }
    write(A, B)

    { Pertukaran nilai A dan B }
    A ← A + B { Jumlahkan nilai A dan B }
    B ← A - B { nilai B adalah pengurangan dengan nilai A diatas }
    A ← A - B { nilai A adalah pengurangan dengan nilai B diatas }

    { cetak nilai A dan B hasil pertukaran }
    write(A, B)
```
