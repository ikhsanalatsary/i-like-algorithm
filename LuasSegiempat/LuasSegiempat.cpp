//g++  5.4.0
/** PROGRAM Luas Segiempat 
 * Program membaca panjang (p) dan lebar (l) sebuah segiempat yang berbentuk
 * persegi panjang, menghitung luas segiempat, lalu mencetaknya ke layar.
 */

#include <iostream>
using namespace std;

int main()
{
    // DEKLARASI
    float p;
    float l;
    float Luas;
    
    // ALGORITMA
    cin >> p >> l;
    Luas = p * l;
    cout << "p = " << p << "\nl = " << l << "\nLuas Segiempat adalah " << Luas;
}
