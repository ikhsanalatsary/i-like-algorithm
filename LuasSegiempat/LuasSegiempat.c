//gcc 5.4.0
/** PROGRAM Luas Segiempat 
 * Program membaca panjang (p) dan lebar (l) sebuah segiempat yang berbentuk
 * persegi panjang, menghitung luas segiempat, lalu mencetaknya ke layar.
 */


#include  <stdio.h>

int main(void)
{
    // DEKLARASI
    float p;
    float l;
    float Luas;
    
    // ALGORITMA
    scanf("%f %f", &p, &l);
    printf("p = %f \nl = %f\n", p, l);
    Luas = p * l;
    printf("Luas Segiempat adalah %f cm²", Luas);
    return 0;
}
