//fpc 3.0.0

program LuasSegiempat;
{ Program membaca panjang (p) dan lebar (l) sebuah segiempat yang berbentuk
  persegi panjang, menghitung luas segiempat, lalu mencetaknya ke layar.}

{ DEKLARASI }
var p: real;    { pamjang segiempat, dalam satuan cm }
var l: real;    { lebar segiempat, dalam satuan cm }
var Luas: real; { luas segiempat, dalam satuan cm² }

{ ALGORITMA }
begin
    readln(p, l);
    Luas := p * l;
    writeln('Luas Segiempat adalah', Luas, ' cm²');
end.
