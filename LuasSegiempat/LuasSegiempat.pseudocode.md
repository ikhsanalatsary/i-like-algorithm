# Luas Segiempat

## Notasi

```
 PROGRAM Luas Segiempat 
  { Program membaca panjang (p) dan lebar (l) sebuah segiempat yang berbentuk
  persegi panjang, menghitung luas segiempat, lalu mencetaknya ke layar.}
  
  DEKLARASI:
    p : real;    { pamjang segiempat, dalam satuan cm }
    l : real;    { lebar segiempat, dalam satuan cm }
    Luas : real; { luas segiempat, dalam satuan cm² }
    
  ALGORITMA:
    read(p, l)   { inputkan panjang dan lebar segiempat }
    Luas ← p * l { hitung luas segiempat }
    write(Luas)  { tampilkan luas segiempat ke layar }

```