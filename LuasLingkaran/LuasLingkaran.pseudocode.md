# Luas Lingkaran

## Notasi
```
  PROGRAM Luas Lingkaran
  { Program membaca panjang jari-jari (r) } sebuah lingkaran, menghitung luas
  lingkaran, lalu mencetaknya ke layar.}
  
  DEKLARASI:
    const PI = 3.14 { konstanta π }
    r : real;       { jari-jari lingkaran, dalam satuan cm }
    L : real;       { luas lingkaran, dalam satuan cm² }
    
  ALGORITMA:
    read(r)        { inputkan jari-jari lingkaran }
    L ← PI * r * r { hitung luas lingkaran }
    write(L)       { tampilkan luas lingkaran ke layar }
```
