//fpc 3.0.0

program LuasLingkaran;
{ Program membaca panjang jari-jari (r) sebuah lingkaran, menghitung luas lingkaran,
 lalu mencetak luas tersebut ke layar }

{ DEKLARASI }
uses crt;
const pi = 3.14; { konstanta pi }
var r: real; { jari-jari lingkaran dalam satuan cm }
    L: real; { luas lingkaran dalam satuan cm2 }

{ ALGORITMA }
begin
    readln(r);                                { inputkan jari-jari lingkaran }
    write('r = ', r, ' ');
    L := pi * r * r;                          { hitung luas lingkaran }
    writeln('Luas lingkaran = ', L, ' cm^2'); { tampilkan luas lingkaran ke layar }
end.
