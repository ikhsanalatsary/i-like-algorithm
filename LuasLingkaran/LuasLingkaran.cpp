//g++  5.4.0

// PROGRAM Luas Lingkaran
// Program membaca panjang jari-jari (r) sebuah lingkaran, menghitung luas
// lingkaran, lalu mencetaknya ke layar.

#include <iostream>
using namespace std;

int main()
{
    // DEKLARASI
    const float pi = 3.14; // konstanta pi
    float r, L;
    
    // ALGORITMA
    cin >> r;
    cout << " jika r = " << r << " ";
    L = pi * r * r;
    cout << "Maka Luas lingkaran = " << L << " cm^2 ";
}
