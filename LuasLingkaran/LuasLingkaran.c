//gcc 5.4.0
// PROGRAM Luas Lingkaran
// Program membaca panjang jari-jari (r) sebuah lingkaran, menghitung luas
// lingkaran, lalu mencetaknya ke layar.

#include  <stdio.h>

int main()
{
    // DEKLARASI
    #define pi 3.14 // konstanta pi
    float r, L; // r = jari-jari dalam satuan cm & L = luas lingkaran dalam satuan cm2
    
    // ALGORITMA
    scanf("%f", &r);
    printf("r = %f. ", r);
    L = pi * r * r;
    printf("Maka Luas lingkaran %f cm^2", L);
    return 0;
}
